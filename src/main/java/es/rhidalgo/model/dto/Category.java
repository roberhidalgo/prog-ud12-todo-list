package es.rhidalgo.model.dto;

import es.rhidalgo.exceptions.WrongCategoryException;
import javafx.concurrent.Worker;

public enum Category {
    HOUSEWORK() {
        @Override
        public String toString() {
            return "Hogar";
        }
    },
    HOMEWORK() {
        @Override
        public String toString() {
            return "Clase";
        }
    },
    PLAY() {
        @Override
        public String toString() {
            return "Jugar";
        }
    };

    public static Category getCategory(String categoryType) throws WrongCategoryException {
        if (categoryType.equals(Category.HOUSEWORK.toString())) {
            return Category.HOUSEWORK;
        } else if (categoryType.equals(Category.HOMEWORK.toString())) {
            return Category.HOMEWORK;
        } else if (categoryType.equals(Category.PLAY.toString())) {
            return Category.PLAY;
        } else {
            throw new WrongCategoryException(categoryType);
        }
    }
}