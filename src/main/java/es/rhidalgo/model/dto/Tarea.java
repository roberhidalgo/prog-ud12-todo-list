package es.rhidalgo.model.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Tarea {

    private int id;

    private String descripcion;

    private LocalDateTime fechaAlta;

    private boolean finalizada;

    private Category category;

    public Tarea(int id, String descripcion, Category category) {
        this(id, descripcion, LocalDateTime.now(), false, category);
    }

    public Tarea(int id, String descripcion, LocalDateTime fechaAlta, boolean finalizada, Category category) {
        this.id = id;
        this.descripcion = descripcion;
        this.fechaAlta = fechaAlta;
        this.finalizada = finalizada;
        this.category = category;
    }

    @Override
    public String toString() {
        return this.id + "-" + this.descripcion + "-" + getFechaAlta() + "-" + false;
    }

    public int getId() {
        return id;
    }

    public String getFechaAlta() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        return dateTimeFormatter.format(this.fechaAlta);
    }

    public LocalDate getFechaAltaSinTiempo(){
        return this.fechaAlta.toLocalDate();
    }

    public boolean contieneTexto(String texto) {
        return this.descripcion.startsWith(texto);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public boolean isFinalizada() {
        return finalizada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tarea tarea = (Tarea) o;
        return id == tarea.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public boolean tieneEsteID(int id) {
        return this.id == id;
    }

    public void cambiaEstado() {
        this.finalizada = !this.finalizada;
    }

    public Category getCategory() {
        return category;
    }
}
