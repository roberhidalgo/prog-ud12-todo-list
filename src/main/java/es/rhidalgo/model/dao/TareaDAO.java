package es.rhidalgo.model.dao;

import es.rhidalgo.exceptions.DatabaseErrorException;
import es.rhidalgo.model.dto.Tarea;

import java.util.ArrayList;

public interface TareaDAO {

    ArrayList<Tarea> findAll() throws DatabaseErrorException;

    ArrayList<Tarea> findAll(String texto) throws DatabaseErrorException;

    boolean save(Tarea tarea) throws DatabaseErrorException;
}
