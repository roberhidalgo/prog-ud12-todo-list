package es.rhidalgo.model.dao;

import es.rhidalgo.controller.ChangeScene;
import es.rhidalgo.exceptions.DatabaseErrorException;
import es.rhidalgo.exceptions.WrongCategoryException;
import es.rhidalgo.model.dto.Category;
import es.rhidalgo.model.dto.Tarea;
import javafx.scene.chart.PieChart;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class FilesTareaDAO implements TareaDAO {

    private static final String NOMBRE_FICHERO = "resources/database/tareas.txt";

    private final File file;

    public FilesTareaDAO() {
        this.file = new File(NOMBRE_FICHERO);
    }

    @Override
    public ArrayList<Tarea> findAll() {

        ArrayList<Tarea> tareas = new ArrayList<>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            do {
                String lineaTarea = bufferedReader.readLine();
                if (lineaTarea == null)
                    break;

                tareas.add(getTareaFromRegister(lineaTarea));
            }while(true);
        } catch (WrongCategoryException ex) {
            System.out.println(ex.getMessage());
        }
        catch (FileNotFoundException ex) {
            System.out.println("El fichero no existe");
        } catch (IOException ex) {
            System.out.println("Error de lectura");
        }

        return tareas;
    }

    @Override
    public ArrayList<Tarea> findAll(String texto) {

        ArrayList<Tarea> tareasFiltradas = new ArrayList<>();

        ArrayList<Tarea> tareas = findAll();
        for (Tarea tarea: tareas) {
            if (tarea.contieneTexto(texto)) {
                tareasFiltradas.add(tarea);
            }
        }

        return tareasFiltradas;
    }

    private Tarea findById(int id) throws DatabaseErrorException {

        try (BufferedReader bufferedReader = getReader()) {
            do {
                try {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        return null;
                    }
                    Tarea tarea = getTareaFromRegister(reservaRegistro);
                    if (tarea.getId() == id) {
                        return tarea;
                    }
                } catch (WrongCategoryException ex) {
                    System.out.println(ex.getMessage());
                }
            } while (true);
        }
        catch (IOException ex) {
            throw new DatabaseErrorException();
        }
    }

    private BufferedReader getReader() throws IOException {
        return new BufferedReader(new FileReader(file));
    }

    @Override
    public boolean save(Tarea tarea) throws DatabaseErrorException {
        try {
            if (findById(tarea.getId()) == null) {
                append(tarea);
                return true;
            }
            return updateOrRemove(tarea, true);
        } catch (IOException ex) {
            throw new DatabaseErrorException();
        }
    }

    private void append(Tarea tarea) throws IOException {
        try (BufferedWriter bufferedWriter = getWriter(true)) {
            bufferedWriter.write(getRegisterFromTarea(tarea));
            bufferedWriter.newLine();
        }
    }

    private Tarea getTareaFromRegister(String reservaRegister) throws WrongCategoryException {
        String[] reservaFields = reservaRegister.split("-");
        int id = Integer.parseInt(reservaFields[0]);
        String descripcion = reservaFields[1];
        LocalDateTime fecha = getFecha(reservaFields[2]);
        boolean realizada = Boolean.parseBoolean(reservaFields[3]);
        Category category = Category.getCategory(reservaFields[4]);
        return new Tarea(id, descripcion, fecha, realizada, category);
    }

    private LocalDateTime getFecha(String fecha) {
        return LocalDateTime.parse(fecha, DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm"));
    }

    private String getRegisterFromTarea(Tarea tarea) {

        String[] fields = new String[5];
        fields[0] = String.valueOf(tarea.getId());
        fields[1] = tarea.getDescripcion().trim();
        fields[2] = tarea.getFechaAlta().trim();
        fields[3] = String.valueOf(tarea.isFinalizada());
        fields[4] = String.valueOf(tarea.getCategory());
        return String.join("-", fields);
    }

    private boolean updateOrRemove(Tarea tarea, boolean update) throws IOException, DatabaseErrorException {
        ArrayList<Tarea> reservaslist = findAll();
        try (BufferedWriter bufferedWriter = getWriter(false)) {
            for (Tarea reservaItem : reservaslist) {
                if (!reservaItem.equals(tarea)) {
                    bufferedWriter.write(getRegisterFromTarea(reservaItem));
                    bufferedWriter.newLine();
                } else if (update) {
                    bufferedWriter.write(getRegisterFromTarea(tarea));
                    bufferedWriter.newLine();
                }
            }
        }
        return true;
    }

    private BufferedWriter getWriter(boolean append) throws IOException {
        return new BufferedWriter(new FileWriter(file, append));
    }
}
