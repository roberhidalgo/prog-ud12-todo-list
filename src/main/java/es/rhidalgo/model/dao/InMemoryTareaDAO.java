package es.rhidalgo.model.dao;

import es.rhidalgo.model.dto.Category;
import es.rhidalgo.model.dto.Tarea;

import java.util.ArrayList;

public class InMemoryTareaDAO implements TareaDAO {

    private ArrayList<Tarea>tareas;

    public InMemoryTareaDAO() {
        this.tareas = new ArrayList<>();
        setTestData();
    }

    @Override
    public ArrayList<Tarea> findAll() {
        return this.tareas;
    }

    @Override
    public ArrayList<Tarea> findAll(String texto) {
        ArrayList<Tarea>tareasFiltradas = new ArrayList<>();
        for (Tarea tarea: tareas) {
            if (tarea.contieneTexto(texto)) {
                tareasFiltradas.add(tarea);
            }
        }

        return tareasFiltradas;
    }

    private void setTestData() {
        this.tareas.add(new Tarea(1, "Bajar la basura", Category.HOUSEWORK));
        this.tareas.add(new Tarea(2, "Poner la lavadora", Category.HOUSEWORK));
        this.tareas.add(new Tarea(3, "Hacer la comida", Category.HOUSEWORK));
    }

    @Override
    public boolean save(Tarea tarea) {
        return this.tareas.add(tarea);
    }
}
