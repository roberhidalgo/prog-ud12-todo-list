package es.rhidalgo.exceptions;

public class WrongCategoryException extends Exception {

    public WrongCategoryException(String categoria) {
        super ("La categoría '" + categoria + "' no existe");
    }
}
