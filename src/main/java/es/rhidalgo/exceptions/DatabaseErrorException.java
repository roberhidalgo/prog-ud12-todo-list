package es.rhidalgo.exceptions;

public class DatabaseErrorException extends Exception{

    public DatabaseErrorException() {
        super("Error al intentar conectar con la capa de persistencia.");
    }
}
