package es.rhidalgo;

import es.rhidalgo.controller.ChangeScene;
import es.rhidalgo.controller.TareaController;
import es.rhidalgo.model.dao.FilesTareaDAO;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {

        FilesTareaDAO filesTareaDAO = new FilesTareaDAO();
        TareaController tareaController = new TareaController(filesTareaDAO);

        ChangeScene.change(stage, tareaController, "/views/tarea_list.fxml");
    }

    public static void main(String[] args) {
        launch();
    }

}