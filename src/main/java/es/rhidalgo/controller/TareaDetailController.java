package es.rhidalgo.controller;

import es.rhidalgo.controller.ChangeScene;
import es.rhidalgo.controller.GenericController;
import es.rhidalgo.controller.TareaController;
import es.rhidalgo.exceptions.DatabaseErrorException;
import es.rhidalgo.model.dao.TareaDAO;
import es.rhidalgo.model.dto.Tarea;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.EventObject;

public class TareaDetailController implements GenericController {

    @FXML
    private TextField textFieldDescripcion;

    @FXML
    private DatePicker datePickerFecha;

    @FXML
    private CheckBox checkBoxRealizado;


    private final TareaDAO tareaDAOInterfaz;
    private Tarea tarea;

    public TareaDetailController(TareaDAO tareaDAO, Tarea tarea) {
        this.tareaDAOInterfaz = tareaDAO;
        this.tarea = tarea;
    }

    @FXML
    private void handleChangeInRealizada(ActionEvent event) {
        try {
            this.tarea.cambiaEstado();
            this.tareaDAOInterfaz.save(tarea);
        } catch (DatabaseErrorException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void handleButtonBack(ActionEvent event) {
        try {
            Stage stage = (Stage) ((Node) ((EventObject) event).getSource()).getScene().getWindow();
            TareaController tareaController = new TareaController(tareaDAOInterfaz);
            ChangeScene.change(stage, tareaController, "/views/tarea_list.fxml");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void initialize() {
        textFieldDescripcion.setText(tarea.getDescripcion());

        datePickerFecha.setEditable(false);
        datePickerFecha.setOnMouseClicked(e -> {
            if(!datePickerFecha.isEditable()) {
                datePickerFecha.hide();
            }
        });

        datePickerFecha.setValue(tarea.getFechaAltaSinTiempo());

        this.checkBoxRealizado.setSelected(this.tarea.isFinalizada());
    }






}
