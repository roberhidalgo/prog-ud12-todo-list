package es.rhidalgo.controller;

import es.rhidalgo.exceptions.DatabaseErrorException;
import es.rhidalgo.model.dao.TareaDAO;
import es.rhidalgo.model.dto.Category;
import es.rhidalgo.model.dto.Tarea;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventObject;

public class TareaController implements GenericController {

    @FXML
    private ListView<Tarea> taskListView;
    @FXML
    private TextField textNewTask;
    @FXML
    private TextField searchBar;
    @FXML
    private ComboBox<Category> categorySelector;

    private final TareaDAO tareaDAOInterfaz;

    public TareaController(TareaDAO tareaDAO) {
        this.tareaDAOInterfaz = tareaDAO;
    }

    @FXML
    public void initialize() {
        taskListView.setItems(getData());
        categorySelector.setItems(FXCollections.observableArrayList(Category.values()));
        taskListView.setCellFactory((ListView<Tarea> l) -> new TaskListViewCellController());
    }

    private ObservableList<Tarea> getData() {
        try {
            return FXCollections.observableArrayList(tareaDAOInterfaz.findAll());
        } catch (DatabaseErrorException ex) {
            System.out.println(ex.getMessage());
            return FXCollections.observableArrayList(new ArrayList<>());
        }
    }

    @FXML
    private void addNewTask() {
        try {
            Tarea tarea = new Tarea(taskListView.getItems().size() + 1, textNewTask.getText(), categorySelector.getValue());
            tareaDAOInterfaz.save(tarea);
            taskListView.getItems().add(tarea);
            taskListView.scrollTo(tarea);
            textNewTask.setText("");
        } catch (DatabaseErrorException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void filtrarTareas() {
        try {
            taskListView.getItems().clear();
            String texto = searchBar.getText();
            System.out.println(texto);
            ArrayList<Tarea> tareas = tareaDAOInterfaz.findAll(texto);
            System.out.println(Arrays.toString(tareas.toArray()));
            taskListView.getItems().addAll(tareas);
        }catch (DatabaseErrorException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void handleSelectedItem(MouseEvent event) {

        /*
            El siguiente código, cambia el estado de una tarea. Es un código alternativo al que está comentado

            Tarea tarea = taskListView.getSelectionModel().getSelectedItem();
            tarea.cambiaEstado();
            this.tareaDAOInterfaz.save(tarea);
            taskListView.getSelectionModel().clearSelection();
            taskListView.refresh();
        */

        try {
            Stage stage = (Stage) ((Node) ((EventObject) event).getSource()).getScene().getWindow();
            Tarea tarea = taskListView.getSelectionModel().getSelectedItem();
            TareaDetailController tareaDetailController = new TareaDetailController(tareaDAOInterfaz, tarea);
            ChangeScene.change(stage, tareaDetailController, "/views/tarea_detail.fxml");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

