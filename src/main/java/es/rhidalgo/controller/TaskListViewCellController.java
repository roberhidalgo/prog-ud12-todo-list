package es.rhidalgo.controller;

import es.rhidalgo.model.dto.Tarea;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URISyntaxException;

public class TaskListViewCellController extends ListCell<Tarea> {

    @FXML
    private AnchorPane root;

    @FXML
    private Label descriptionLabel;

    @FXML
    private ImageView statusImage;

    @FXML
    private ImageView categoryImageView;


    public TaskListViewCellController() {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/list_item.fxml"));
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void updateItem(Tarea tarea, boolean empty) {

        super.updateItem(tarea, empty);

        if (empty) {
            setGraphic(null);
        } else {
            descriptionLabel.setText(tarea.getDescripcion());

            if (tarea.isFinalizada()) {
                statusImage.setImage(new Image(getPathImage("/images/done.png")));
            } else {
                statusImage.setImage(new Image(getPathImage("/images/pending.png")));;
            }

            categoryImageView.setImage(getCategoryImage(tarea));

            setGraphic(root);
        }
    }

    private String getPathImage(String fileName) {
        try {
            return getClass().getResource(fileName).toURI().toString();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private Image getCategoryImage(Tarea tarea) {
        switch (tarea.getCategory()) {
            case PLAY:
                return new Image(getPathImage("/images/play.png"));
            case HOMEWORK:
                return new Image(getPathImage("/images/homework.png"));
            case HOUSEWORK:
                return new Image(getPathImage("/images/housework.png"));
            default:
                return null;
        }
    }
}
